import { Request, Response } from 'express'

import Nodejs, { INodejs } from '../models/Nodejs';

const cron = require('node-cron');

var rp = require('request-promise');

export const getApiPublic = async (req: Request, res: Response) => {
    var API = process.env.API_PUBLIC
    console.log(API)
    const nodejs = await Nodejs.find({}).lean();
    if (!nodejs) return res.status(404).json('No User found');
    res.json(nodejs);
}

export function autoPost() {
    cron.schedule("0 * * * *", () => {
        const defaultRequest = rp.defaults({
            baseUrl: process.env.API_PUBLIC,
            json: true,
        })
        const URL = process.env.API_LOCAL + 'api/node/nodejs'
        defaultRequest({
            url: '/search_by_date?query=nodejs',
            method: 'GET',
            json: true
        }).then((body: any) => {
            const hits = body.hits
            hits.forEach((hit: any) => {
                const payload = {
                    story_title: hit.story_title,
                    story_url: hit.story_url,
                    title: hit.title,
                    author: hit.author,
                    created_at: hit.created_at
                };
                console.log(payload)
                rp({
                    url: URL,
                    method: 'POST',
                    body: payload,
                    json: true
                }).then((body: any) => console.log(body))
                    .catch((err: any) => {
                        console.log(err.name, err.statusCode)
                    });
            })
                .catch((err: any) => {
                    console.log(err.name, err.statusCode)
                })
        });
    })
}

export const saveNodejs = async (req: Request, res: Response) => {
    //saving 
    const nodejs: INodejs = new Nodejs({
        story_title: req.body.story_title,
        story_url: req.body.story_url,
        title: req.body.title,
        author: req.body.author,
        created_at: req.body.created_at
    });
    await nodejs.save();

    res.json(nodejs);

}

export const getNodejs = async (req: Request, res: Response) => {
    const nodejs = await Nodejs.find({}).lean();
    if (!nodejs) return res.status(404).json('No User found');
    res.json(nodejs);
}

export const deleteNodejs = async (req: Request, res: Response) => {
    res.send('deleting')
    const { id } = req.params;
    await Nodejs.findByIdAndRemove(id);

}

