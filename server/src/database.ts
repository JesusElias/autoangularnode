import mongoose, { mongo } from 'mongoose'

// const dbHost = 'mongodb://database/autoangularnode';

mongoose.connect('mongodb://localhost/test',{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
})
.then(db => console.log('Database is connected'))
.catch(err => console.log(err));