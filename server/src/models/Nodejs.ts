import {Schema, model, Document} from 'mongoose';

export interface INodejs extends Document {
    story_title: string;
    story_url: string,
    title: string;
    author: string;
    created_at: Date;
}

const nodejsSchema = new Schema({
    story_title: { type: String },
    story_url: { type: String },
    title: { type: String },
    author: { type: String },
    created_at: { type: Date },
});

export default model<INodejs>('Nodejs' , nodejsSchema);
