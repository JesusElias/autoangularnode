import { Router } from 'express'

const router: Router = Router();

import { getApiPublic, saveNodejs, getNodejs, deleteNodejs } from '../controllers/nodejs.Controller'

router.route('/nodejs')
        .get(getApiPublic)
        .get(getNodejs)
        .post(saveNodejs)

// router.post('/saveNodejs', saveNodejs)

// router.get('/getNodejs', getNodejs)

router.route('/nodejs/:id')
        .delete(deleteNodejs)
        
export default router;