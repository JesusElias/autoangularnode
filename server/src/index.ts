import app from './app';
import dotenv from 'dotenv'
const cron = require('node-cron');
import { autoPost } from './controllers/nodejs.Controller'

var rp = require('request-promise');

dotenv.config();

import './database';
import { json } from 'express';

function main() {

    app.listen(app.get('port'));
    console.log('server on port', app.get('port'))

    autoPost();

}

main();


