"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const producto_Controller_1 = require("../controllers/producto.Controller");
router.route('/producto')
    .get(producto_Controller_1.getProducto)
    .post(producto_Controller_1.saveProducto);
// router.post('/saveProducto', saveProducto)
// router.get('/getProducto', getProducto)
router.route('/producto/:id')
    .delete(producto_Controller_1.deleteProducto)
    .put(producto_Controller_1.updateProducto);
exports.default = router;
//# sourceMappingURL=producto.js.map