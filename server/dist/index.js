"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./app"));
const dotenv_1 = __importDefault(require("dotenv"));
const cron = require('node-cron');
const nodejs_Controller_1 = require("./controllers/nodejs.Controller");
var rp = require('request-promise');
dotenv_1.default.config();
require("./database");
function main() {
    app_1.default.listen(app_1.default.get('port'));
    console.log('server on port', app_1.default.get('port'));
    nodejs_Controller_1.autoPost();
}
main();
//# sourceMappingURL=index.js.map